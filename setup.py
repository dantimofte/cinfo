import os
from setuptools import setup, Command


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info')


setup(
    name='cinfo',
    version='1.0.3',
    description='telegram bot for crypto information',
    author='Dan Timofte',
    author_email='dantimofte@me.com',
    url='https://github.com/dantimofte/cinfo',
    packages=['cinfo'],
    install_requires=[
        "aiogram",
        "aiohttp",
        "aiodns",
        "cchardet",
        "aiokraken==1.0.3"
    ],
    cmdclass={
        'clean': CleanCommand,
    },
    entry_points={
        "console_scripts": [
            "cinfo = cinfo.bot:main"
        ],
    }

)
