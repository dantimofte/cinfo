import os
import logging
import pkg_resources
import asyncio
from dataclasses import dataclass, field
from aiokraken import RestClient

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = os.environ.get('TELEGRAM_TOKEN')

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)
version = pkg_resources.require("cinfo")[0].version
formated_message = f"Start cinfo version {version}"

asyncio.get_event_loop().create_task(
    bot.send_message(11778500, formated_message)
)

@dataclass
class TickItem:
    """ Handle Kraken tickitem
        data dict :
        a = ask array(<price>, <whole lot volume>, <lot volume>),
        b = bid array(<price>, <whole lot volume>, <lot volume>),
        c = last trade closed array(<price>, <lot volume>),
        v = volume array(<today>, <last 24 hours>),
        p = volume weighted average price array(<today>, <last 24 hours>),
        t = number of trades array(<today>, <last 24 hours>),
        l = low array(<today>, <last 24 hours>),
        h = high array(<today>, <last 24 hours>),
        o = today's opening price
    """
    symbol: str
    data: dict
    bid: str = field(init=False)
    bid_size: float = field(init=False)
    ask: str = field(init=False)
    ask_size: float = field(init=False)
    last_price: float = field(init=False)
    volume: float = field(init=False)
    high: str = field(init=False)
    low: str = field(init=False)
    base_currency: str = field(init=False)
    quote_currency: str = field(init=False)

    def __post_init__(self):
        self.bid = str(self.data['b'][0])
        self.bid_size = float(self.data['b'][2])
        self.ask = str(self.data['a'][0])
        self.ask_size = float(self.data['a'][2])
        self.last_price = float(self.data['c'][0])
        self.volume = float(self.data['v'][1])
        self.high = str(self.data['h'][1])
        self.low = str(self.data['l'][1])
        self.base_currency = self.symbol[1:4]
        self.quote_currency = self.symbol[5:]
        self.data = None


@dp.message_handler(commands=['doge'])
async def get_doge_ticker(message: types.Message):
    """
    """
    rest_kraken = RestClient()
    data = {
        "pair": "XDGXBT"
    }
    response = await rest_kraken.ticker(data)
    for ticker_pair, ticker_data in response.items():
        tick_item = TickItem(ticker_pair, ticker_data)
        print(tick_item)
        formated_message = (
            "<pre>"
            f"ASK : {tick_item.ask[:10]} @ {tick_item.ask_size:,}\n"
            f"BID : {tick_item.bid[:10]} @ {tick_item.bid_size:,}\n"
            f'24 hour : {tick_item.low[:10]} - {tick_item.high[:10]}'
            "</pre>"
        )
        await bot.send_message(message.chat.id, formated_message, parse_mode='HTML')
    await rest_kraken.close()


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when client send `/start` or `/help` commands.
    """
    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")


@dp.message_handler(regexp='(^cat[s]?$|puss)')
async def cats(message: types.Message):
    with open('data/cats.jpg', 'rb') as photo:
        await bot.send_photo(message.chat.id, photo, caption='Cats is here 😺',
                             reply_to_message_id=message.message_id)


@dp.message_handler()
async def echo(message: types.Message):
    chatid = message.chat.id
    await bot.send_message(message.chat.id, formated_message)
    await bot.send_message(chatid, f'chatid = {chatid}')


def main():
    executor.start_polling(dp, skip_updates=True)
